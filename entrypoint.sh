#!/usr/bin/env bash

mkdir -p ~/.ssh

cp /ansible/known_hosts ~/.ssh/
cp /ansible/config ~/.ssh/
cp /ansible/hosts /etc/ansible/hosts

if [ -e /ansible/ansible.cfg ]
 then cp /ansible/ansible.cfg /etc/ansible/ansible.cfg
fi

chmod -R 600 ~/.ssh

# check if the first argument passed in looks like a flag
if [ "$(printf %c "$1")" = '-' ]; then
  set -- /sbin/tini -- ansible "$@"
# check if the first argument passed in is ansible-playbook
elif [ "$1" = 'ansible-config' ] || [ "$1" = 'ansible-connection' ]  || [ "$1" = 'ansible-doc' ] || [ "$1" = 'ansible-galaxy' ] || [ "$1" = 'ansible-inventory' ] || [ "$1" = 'ansible-playbook' ] || [ "$1" = 'ansible-pull' ] || [ "$1" = 'ansible-vault' ] || [ "$1" = 'ansible' ]; then
  set -- /sbin/tini -- "$@"
fi

exec "$@"